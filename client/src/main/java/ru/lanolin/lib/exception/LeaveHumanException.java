package ru.lanolin.lib.exception;

public class LeaveHumanException extends RuntimeException {

    private String message;

	public LeaveHumanException() {
        message = "Выгонять некого с таким профилем. Даже клона нет.";
    }

	public LeaveHumanException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
