package ru.lanolin.lib.exception;

public class AddHumanException extends RuntimeException {

    private String message;

	public AddHumanException() {
        message = "Добавили участника, который уже есть на мероприятии. Клонирование не позволительно и запрещено.";
    }

	public AddHumanException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
