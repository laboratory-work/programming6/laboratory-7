package ru.lanolin.lib.fete;

@FunctionalInterface
public interface DoLater {

    /**
     * Требуемое действие, отсчитывваемое от начачло мероприятия
     *
     * @param reactionTimeMillis
     * 		когда требуется запустить
     */
    void complete(long reactionTimeMillis);

}
