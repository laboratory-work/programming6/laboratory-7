package ru.lanolin.lib.fete;

import ru.lanolin.lib.enums.Phrases;
import ru.lanolin.lib.enums.Situation;
import ru.lanolin.lib.exception.CountOfPeopleException;
import ru.lanolin.lib.human.Human;
import ru.lanolin.util.Utils;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Класс, описывающий Мероприятие
 */
public abstract class Fete implements DoFete {

    /**
     * Коллекция, в которой храянтся все {@link Human}
     */
    protected final List<Human> guests;
    /**
     * {@link ArrayList} в котором храняться действия, которые необходимо сделать через определенный промежуток времени.
     */
    private ArrayList<DoLater> doLater = new ArrayList<>();

    /**
     * Текущая обстановка на мероприятии
     */
    private Situation situation;
    /**
     * Началось мероприятие или нет
     */
    private boolean startFete = false;

    /**
     * Контроллер мероприятия. Отвечает за выполенения действий
     */
    private final Controller controller;

    public Fete(Situation situation){
        this.guests = new CopyOnWriteArrayList<>(new ArrayList<>(Utils.Const.SIZE));
        this.situation = situation;
        controller = new Controller();
    }

    /**
     * Метод, возвращающий {@link List} гостей, присутствующих на мероприятии.
     *
     * @return {@link List}
     */
    public List<Human> getGuests() {
        return guests;
    }

    /**
     * Получаем ситуацию
     * @return {@link Situation}
     */
    public Situation getSituation() {
        return situation;
    }

    /**
     * Добавляем действие, которое неоюходимо сделать через определенный промежуток времени.
     * @param doLate {@link DoLater}
     */
    public void addDoLater(DoLater doLate) {
        if (!doLater.contains(doLate) && doLate != null)
            doLater.add(doLate);
    }

    /**
     * Устанавливает обстановку на мероприятии
     * @param s {@link Situation} другая обстановка
     */
    public void setSituation(Situation s) {
        situation = s;
        System.out.println(">>Окружающая обстановка изменилась на " + s);
        if (startFete && s != Situation.RAIN) {
            List<Human> getRandomPeople = Utils.pickNRandomElements(guests);
            Human h1 = getRandomPeople.get(0);
            Human h2 = getRandomPeople.get(1);
            h1.repeat(h2, Phrases.FEELGOOD);
        }
    }

    /**
     * Выполняет один проход по рандомнуму действию
     * @param time {@link Long} текущее время.
     */
    private void tick(long time) {

        if ((time / Utils.Const.SECOND) > Utils.Const.WorkTimeS) {
            this.stopFate();
            return;
        }

        for (DoLater dl : doLater)
            dl.complete(time / 1000000);

        List<Human> getRandomPeople = Utils.pickNRandomElements(guests);
        Human h1 = getRandomPeople.get(0);
        Human h2 = getRandomPeople.get(1);

        int randDO = new Random().nextInt(4);

        if (randDO == 0) {
            h1.great(h2);
        } else if (randDO == 1) {
            h1.bowOut(h2);
        } else if (randDO == 2) {
            h1.rateOutfit(h2);
        } else if (randDO == 3) {
            Phrases phr = Utils.getRandomElementEnum(Phrases.class);
            do {
                if (getSituation() == Situation.RAIN && phr == Phrases.FEELGOOD)
                    phr = Utils.getRandomElementEnum(Phrases.class);
                else break;
            } while (true);
            h1.repeat(h2, phr);
        }
    }

    /**
     * Получает, в каком состоянии находится мероприятие
     * @return {@link Boolean}
     */
    public boolean isStartFete() {
        return startFete;
    }

    /**
     * Установить началось ли мероприятие
     * @param startFete {@link Boolean}
     */
    public void setStartFete(boolean startFete) {
        this.startFete = startFete;
    }

    /**
     * Получаем количество участников на мероприятии
     * @return {@link Integer}
     */
    public int sizeGuest() {
        return guests.size();
    }

    /**
     * Получаем контроллер управления ммероприятием
     * @return {@link Controller}
     */
    public Controller getController(){
        return controller;
    }

    /**
     * Класс Контроллер. Управляет, когда необходимо запутстить определенный update
     */
    public class Controller {

        private boolean isRunning;

        Controller(){
            isRunning = false;
        }

        public void start() {
            if(isRunning)
                return;
            try {
                startFete();
                run();
            }catch (CountOfPeopleException c){ }
        }

        void stop(){
            if(!isRunning)
                return;
            isRunning = false;
        }

        private void run(){
            isRunning = true;

            final double frameTime = 1.0 / Utils.Const.FRAMECAP;

            long lastTime = Utils.Time.getTime();
            final long startGame = lastTime;
            double unprocessedTime = 0;

            while(isRunning) {
                boolean render = false;
                long startTime = Utils.Time.getTime();
                long passedTime = startTime - lastTime;
                lastTime = startTime;
                unprocessedTime += passedTime / (double) Utils.Const.SECOND;

                while((unprocessedTime - frameTime) > 1e-8) {
                    render = true;
                    unprocessedTime -= frameTime;
                    if(!isStartFete()) { stop(); return; }
                }

                if(render) {
                    tick(lastTime - startGame);
                }else {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        //e.printStackTrace();
                    }
                }
            }
        }
    }
}
