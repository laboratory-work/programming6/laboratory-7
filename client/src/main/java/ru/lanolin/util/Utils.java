package ru.lanolin.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Класс, котором описаны статические методы. <br> Используются для различных утилитарных методов
 *
 * @version 3.0
 */
public class Utils {

	/**
	 * Получает рандомный элемент из {@link Enum}.
	 * @param en {@link Class}
	 * @param <E> тип, который требутеся получить
	 * @return {@link Enum} рандомный элемент
	 */
	public static <E extends Enum<E>> E getRandomElementEnum(Class<E> en) {
		Enum<E>[] all = en.getEnumConstants();
		int random = (int) (Math.random() * all.length);
		return (E) all[random];
	}

	/**
	 * Получаем перемешанных список элементов {@link ru.lanolin.lib.human.Human}
	 *
	 * @param list
	 *        {@link List} входной лист
	 * @param <E>
	 * 		тип класса, которых храниться в коллекции
	 * @return переммешанных список
	 */
	public static <E> List<E> pickNRandomElements(final List<E> list) {
		ArrayList<E> returnList = new ArrayList<>(list);
		for (int i = 0; i < 5; i++) Collections.shuffle(returnList);
		return returnList.subList(returnList.size() - 2, returnList.size());
	}

	/**
	 * Интерфейс, где храняться константы
	 */
	public interface Const {
		/**
		 * Количесво наносекунд в 1 секунде
		 */
		long SECOND = (long) 1e9;

		/**
		 * Количесво действий в секунду
		 */
		double FRAMECAP = 2.0f;

		/**
		 * Максимальныей размер {@link java.util.Vector}, где хранятся все {@link ru.lanolin.lib.human.Human}
		 */
		int SIZE = 256;

		/**
		 * Максимально время работы мероприятия
		 */
		int WorkTimeS = 5;
	}

	/**
	 * Класс, для работы со временем
	 */
	public static class Time {
		/**
		 * Получает текущее время в наносекундах
		 *
		 * @return {@link Long}
		 */
		public static long getTime() {
			return System.nanoTime();
		}

		/**
		 * Проверяет, работает ли сейчас текущее мероприятие
		 *
		 * @param reactionTime {@link Long} текущее время
		 * @return {@link Boolean} возвращает, в каком состоянии находится мероприятие
		 */
		public static boolean isWork(long reactionTime) {
			return reactionTime < (1000 / Const.FRAMECAP) && reactionTime >= 0;
		}
	}
}
