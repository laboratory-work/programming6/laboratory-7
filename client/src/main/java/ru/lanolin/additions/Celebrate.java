package ru.lanolin.additions;

import ru.lanolin.Main;
import ru.lanolin.lib.enums.Situation;
import ru.lanolin.lib.exception.AddHumanException;
import ru.lanolin.lib.exception.CountOfPeopleException;
import ru.lanolin.lib.exception.LeaveHumanException;
import ru.lanolin.lib.fete.Fete;
import ru.lanolin.lib.human.Human;

public class Celebrate extends Fete {

    public Celebrate(Situation situation) {
        super(situation);
    }

    @Override
    public void addParticipant(Human h) {
        if (this.guests.contains(h)) throw new AddHumanException();
        this.guests.add(h);
    }

    @Override
    public void leaveParticipant(Human h) {
        if (!this.guests.contains(h)) throw new LeaveHumanException();
        this.guests.remove(h);
    }

    @Override
    public void startFete() throws CountOfPeopleException {
        System.out.println(">>Начинаем мероприятие");
        if(this.sizeGuest() == 0)
            throw new CountOfPeopleException("Никого нет. Зачем вообще это надо?");
        else if(this.sizeGuest() < 2)
            throw new CountOfPeopleException();
        this.setStartFete(true);
    }

    @Override
    public void stopFate() {
        System.out.println(">>Заканчиваем мероприятие");
        this.setStartFete(false);
        this.leaveParticipants(this.guests);
        Main.client.shutdown();
    }
}
