package ru.lanolin.additions;

import ru.lanolin.lib.human.Human;

public class Guest extends Human {

    private static final long serialVersionUID = 4673673961563758015L;

    @Override
    public void rateOutfit(Human other) {
        System.out.println(">" + this.getName() + " оцениват наряд Гостя " + other.getName() + " на " + other.getQuality());
    }

    @Override
    public void great(Human other) {
        System.out.println(">" + this.getName() + " приветвует " + other.getName());
    }

    @Override
    public void bowOut(Human other) {
        System.out.println(">" + this.getName() + " поклонился " + other.getName());
    }
}