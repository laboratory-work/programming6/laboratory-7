package ru.lanolin.util;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.lanolin.util.Utils.*;

public class UtilsTest extends Assert {

    private final List<String> probeSHA384 = new ArrayList<>();
    private final List<String> validEmail = new ArrayList<>();
    private final List<String> invalidEmail = new ArrayList<>();

    @Before
    public void setupValidEmail() {
        validEmail.addAll(Arrays.asList(
                "demmel@sbcglobal.net",
                "rcwil@live.com",
                "knorr@comcast.net",
                "sriha@gmail.com",
                "bader@gmail.com",
                "engelen@mac.com",
                "kenja@aol.com",
                "blixem@gmail.com",
                "stinson@verizon.net",
                "bester@att.net",
                "citizenl@yahoo.ca",
                "staffelb@comcast.net",
                "ntegrity@mac.com",
                "scitext@optonline.net",
                "lydia@gmail.com"
        ));

        invalidEmail.addAll(Arrays.asList(
                "QHcqsVUPWATR8wJzMYa1",
                "Tc9gMQkXECqdzZedIiBR",
                "DlgxWEiUyw9w6HHBSr6V",
                "vxu6h0qGUHAKZD3GEQpS",
                "FR2aqHaHczsn0V2PdbPr",
                "Sh7Fdt1sWStFiw7DbX6y",
                "JpABtTQAWlZai5FWKNji",
                "uQkoJdQIrl2vPRVzzmOx",
                "YkhnTcWdy9t7WFeYaXjx",
                "Hfnt5cxC3mrbD3Lp8jkg",
                "TJX4iEJhOlQWGniH2EOo",
                "ULpV68klcCDi07YEJd8q",
                "JlYwhYvDx01AnuDRRcUk",
                "LuJZC6YYfbqrdD1bCAJD",
                "tr6lBP3wTdYnrQT4lQ2P",
                "nQjgXCpJ1TxgZUIQVssH",
                "Q0G6Ocl3GJ2oSZ4phmGy",
                "kozw9vJmeEpdIPgAAJsR",
                "Vp2PVW5znLe8YMBqWOmy",
                "23dvkep9Ncseq7teO9SR",
                "SLQqY8VQLJIy6HGJ0XRW",
                "yNvXFi7reZxcUSVcddcL",
                "SO61q6jiN8EtFHcMvP5V",
                "p6F2jUubpeM8rmDggai6",
                "oSx42ThJMriZNC4lbVKU",
                "4hQwTSmGv6NrT4BQYD41",
                "wUFH1jCCdZBcjm2WRewn",
                "AsskHRWGskfjNghZx0D8",
                "OLaZe1ed8CR0uK4VouAM",
                "h3UiCXR464pJNUpfpS5W",
                "NHloGNNxKD09C84TIqrS",
                "tuCCpNe8GAaDslONlaCl",
                "oikY5dEKLH6EjkhI1U0a",
                "snzeuwYcnyw00cKQiwut",
                "eaImZQcDXT6AffvXZX7E"
        ));
    }

    @Before
    public void setupSHA384() {
        probeSHA384.add("12345");
        probeSHA384.add("Arfsas");
        probeSHA384.add("efbee16246fc58c31cb489308792f338a194171e367657e146aac5595cc15161ce34f34c545071867176c155c56c1293");
        probeSHA384.add("artem");
        probeSHA384.add("DJYyN#vV2L");
        probeSHA384.add("58586d7b22289c435a996eaf0650986d6709072e2f21dfa8ada2d51f1f10fe416b5c6e25b871b80a336d5a05cfac0478");
    }

    @After
    public void clearSHA384() {
        probeSHA384.clear();
    }

    @After
    public void clearValidEmail() {
        validEmail.clear();
        invalidEmail.clear();
    }

    @Test
    public void testSHA384() {
        for (int i = 0; i < probeSHA384.size(); i += 3) {
            String hex = getSHA384(probeSHA384.get(i), probeSHA384.get(i + 1));
            assertEquals(hex, probeSHA384.get(i + 2));
        }
    }

    @Test
    public void testValidEmail() {
        for (String ve : validEmail) {
            assertTrue(isValidEmail(ve));
        }
        for (String ive : invalidEmail) {
            assertFalse(isValidEmail(ive));
        }
    }

    @Test
    public void testSQLExcept() {
        List<Integer> notMod = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
        List<Integer> modif = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 8, 9, 10));

//        notMod.removeAll(modif);

        modif.removeAll(notMod);
        System.out.println(modif);
    }
}
