package ru.lanolin;

import ru.lanolin.additions.Celebrate;
import ru.lanolin.server.MailSend;
import ru.lanolin.server.MasterDB;
import ru.lanolin.server.Server;
import ru.lanolin.util.Utils;

import javax.mail.MessagingException;
import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.Properties;

public class Main {

	/**
	 * Объект с основной информацией об мероприятии
	 */
	public static Celebrate celebrate;
	/**
	 * Класс, для работы с базой данных.
	 */
	private static MasterDB database;
	/**
	 * Класс, для прослушивания и отправки сообщения на клиент
	 */
	private static Server server;

	/**
	 * Включается ли режим разработчика.<br> В нем включн просмотр {@link Exception#printStackTrace()}
	 */
    public static boolean isDebug;

    static {
        try {
            Properties properties = new Properties();
            properties.load(Main.class.getClassLoader().getResourceAsStream("application.properties"));
            isDebug = Boolean.parseBoolean(properties.getProperty("application.debug", "false"));
        } catch (IOException e) {
			System.err.println("Ошибка при чтении конфигурационных файлов");
			e.printStackTrace();
        }
    }

    /**
	 * Процедура регистрации нового клиента.
	 * @param br {@link BufferedReader}. Активный reader для консоли
	 * @throws IOException Возникают, при чтении с консоли и появлении IO ошибки.
	 */
	private static void registration(BufferedReader br) throws IOException {
		System.out.println("Процедура регистрация");
        System.out.print("Введите e-mail: ");
        String email = br.readLine().trim();
        System.out.print("Введите логин: ");
        String login = br.readLine().trim();

        boolean check = database.checkAuterization(email, login);

        if (!Utils.isValidEmail(email)) {
            System.err.println("Невалидный e-mail адрес. Просьба ввести корректный e-mail");
            System.out.println("Регистрация прекращена");
            return;
        }

        if (!check) {
            String passwd = Utils.randomAlphaNumeric(8);
            try {
				MailSend.sendMessage(email, login, passwd);
				database.registrationNewAccount(email, login, passwd);
				System.out.println("Успешная регистрация. На почту придет сообщение с паролем");
            } catch (MessagingException e) {
                if (isDebug) e.printStackTrace();
                System.err.println("Неполадки в отправке сообщения. Просьба повторить позже");
                System.err.println("Регистрация прекращена");
            } catch (Exception e) {
                if (isDebug) e.printStackTrace();
                System.err.println(e.getLocalizedMessage());
                System.err.println("Регистрация прекращена");
            }
        } else {
            System.err.println("Пользователь с таким логином или e-mail существует. Регистрация прекращена");
        }
    }

    /**
	 * Процедура авторизации клиента на сервере.
	 * @param br {@link BufferedReader}. Активный reader для консоли
	 * @return {@link Boolean}. {@code true} если успешная аутентификация иначе {@code false}
	 * @throws IOException Возникают, при чтении с консоли и появлении IO ошибки.
	 */
	private static boolean authorization(BufferedReader br) throws IOException {
        System.out.println("Авторизация");
        System.out.print("Введите логин: ");
        String login = br.readLine().trim();
//		Console console = System.console();
//		String password = new String(console.readPassword("Введите пароль: "));
//		console.flush();
//        int idAuth = database.isAutentification(login, password);
        int idAuth = 2;
        if (idAuth != -1) {
			celebrate.setCreator(database, idAuth);
            System.out.println("Успешная авторизация");
            System.out.println("Вы вошли в систему. ДЛя получения справки введите `help`");
            return true;
        } else {
            System.err.println("Fail :< Повторитe еще раз");
            return false;
        }
    }

    /**
	 * Главная точка входа приложения
	 * @param args {@link String[]}
	 */
	public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread(Main::saveAndClose));

        database = new MasterDB();
		server = new Server();

		if (!database.connect()) {
			return;
		}

		celebrate = new Celebrate(database.getAllHumans());
		server.start();

		MailSend.init();

        interactive();
    }

    /**
	 * Метод, для работы в интерактивном режиме в консоли.
	 */
	private static void interactive(){
        System.out.println("Добро пожаловать в приложение. " +
                "Для дальнейшей работы необходимо авторизация. " +
                "По всем вопросам `help` или к команде разработчиков");

		try (BufferedReader console = new BufferedReader(new InputStreamReader(System.in))) {
			boolean startInteract = true;
			boolean isAuthentication = false;

			while (startInteract) {
				String[] input;
				try {
					String raw_input = console.readLine();
					if (Objects.isNull(raw_input)) continue;
					input = raw_input.split("\\s+", 2);
				} catch (IOException e) {
					if (isDebug) e.printStackTrace();
					System.err.println("IO ошибка при чтении введенной строки");
					continue;
				}

				try {
					if (!isAuthentication) {
						switch (input[0].toLowerCase()) {
							case "login":
								isAuthentication = authorization(console);
								break;
							case "signin":
								registration(console);
								break;
							case "help":
								System.out.println("Помощь: ");
								System.out.println(">login - авторизация в приложении");
								System.out.println(">signin - реистрация аккаунта");
								System.out.println(">exit - завершение работы");
								break;
							case "exit":
								System.exit(0);
							default:
								System.err.println("Нет такой команды. Для справки вызовите help");
								break;
						}
					} else {
						switch (input[0].toLowerCase()) {
							case "remove_greater":
								celebrate.remove_greater(input[1]);
								break;
							case "add":
								celebrate.add(input[1]);
								break;
							case "remove":
								celebrate.remove(input[1]);
								break;
							case "insert":
								celebrate.insert(input[1]);
								break;
							case "reorder":
								celebrate.reorder();
								break;
							case "show":
								celebrate.show();
								break;
							case "info":
								celebrate.info();
								break;
							case "help":
								celebrate.help();
								break;
							case "save_in_db":
								database.saveHuman(celebrate);
								break;
							case "logout":
								database.saveHuman(celebrate);
//								celebrate.setCreator(-1);
								System.out.println("Выход из системы");
								isAuthentication = false;
								break;
							case "exit":
							case "close":
								startInteract = false;
								server.interrupt();
								break;
							default:
								System.err.println("Нет такой команды. Для справки вызовите help");
								break;
						}
					}
				} catch (ArrayIndexOutOfBoundsException arr) {
					if (isDebug) arr.printStackTrace();
					System.out.println("Команда " + input[0] + " требует ввода еще один элемент. Для справки введие help");
				} catch (IOException ioe) {
					if (isDebug) ioe.printStackTrace();
					System.out.println("Ошибка при чтении, повторите команду");
				}
			}
		} catch (IOException e) {
			if (isDebug) e.printStackTrace();
			System.err.println("IO ошибка");
		}
    }

    /**
	 * Метод, срабатывающий при закрытии приложения. Последний рывок.
	 */
	private static void saveAndClose(){
        System.out.println("Завершение работы сервера");
        try {
            database.saveHuman(celebrate);
        } catch (Exception e) {
            if (isDebug) e.printStackTrace();
            System.err.println("ВНИМАНИЕ!! Сохраннение не произошло из-за ошибки " + e.getLocalizedMessage());
        }
        if (server != null && server.isAlive() && !server.isInterrupted())
            server.interrupt();
        database.disconnect();
	}

}
