package ru.lanolin.util;

import ru.lanolin.Main;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, котором описаны статические методы. <br> Используются для различных утилитарных методов
 *
 * @version 7.3
 */
public class Utils {

    /**
     * Максимальныей размер {@link java.util.ArrayList}, где хранятся все {@link ru.lanolin.lib.human.Human}
     */
    public static final int MAXSIZE = 256;

    /**
     * Скриптовый движок, используется для преобразований {@code JSON} строк в {@link Map}
     */
    private static final ScriptEngine JAVASCRIPT = new ScriptEngineManager().getEngineByName("javascript");

    /**
     * Набор символов, которые используются в генерации рандомной строки.
     */
    private static final String ALPHA_NUMERIC_STRING = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZ0123456789";

    /**
     * Перец. Используется для хеширования пароля для базы данных
     */
    private static final String PEPPER = "M^5oeu9)o^";

    /**
     * Паттерн, для проверки e-mail адреса на корректность
     */
    private static final String PATTERN_EMAIL = "\\w+([.-]?\\w+)*@\\w+([.-]?\\w+)*\\.\\w{2,4}";

    /**
     * Метод, который преобразует {@code JSON} строку в {@link Map}
     *
     * @param element {@code JSON} строку
     * @return {@link Map}, в котором содержатся ключ-значения из JSON
     */
    public static Map<String, Object> parseJSON(String element) {
        Map<String, Object> result = null;
        try {
            result = (Map<String, Object>) JAVASCRIPT.eval("Java.asJSONCompatible(" + element + ")");
        } catch (Exception e) {
            System.err.println("Внимание!!! Введен неверный формат JSON строки. Исправьте или не продолжайте работать.");
        }
        return result;
    }

    /**
     * Конвертирует сериализуемый объект в {@link ByteBuffer} для последующей отправки через {@link java.net.Socket}
     *
     * @param obj {@link Object} {@code implements {@link Serializable}}
     * @return {@code ByteBuffer} запоненный преобразованным касов дя отправки
     * @throws IOException Возникает, если невозможно преобразовать класс в {@link ByteBuffer}
     */
    public static ByteBuffer convertObject2Buffer(Object obj) throws IOException {
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream outputStreamWriter = new ObjectOutputStream(arrayOutputStream);

        outputStreamWriter.writeObject(obj);
        outputStreamWriter.flush();
        ByteBuffer b = ByteBuffer.wrap(arrayOutputStream.toByteArray());
        outputStreamWriter.close();
        arrayOutputStream.close();

        return b;
    }

    /**
     * Предназначен для замены в строке подстроки ключей на их значения.
     *
     * @param pattern {@link String}. Передается шаблон, где ключи указываются как {@code {key}}
     * @param data    {@link String} и {@link Object}. Вводиться пара ключ-знаение. Строго количесвто кратно 2.
     * @return {@link String} Возвращает строку с заменненными ключами.
     */
    public static String convertTemplate(String pattern, Object... data) {
        if (data.length % 2 != 0) throw new ArrayIndexOutOfBoundsException("Введенео не то кол-во аргументов");

        final HashMap<String, Object> conv = new HashMap<>();
        for (int i = 0; i < data.length; i += 2)
            conv.put(String.valueOf(data[i]), data[i + 1]);

        return convertTemplate(pattern, conv);
    }

    /**
     * Предназначен для замены в строке подстроки ключей на их значения.
     *
     * @param pattern    {@link String} Передается шаблон, где ключи указываются как {@code {key}}
     * @param kv_storage {@link Map} Передаются пары ключ-значение в Map.
     * @return {@link String} Возвращает строку с заменненными ключами
     */
    public static String convertTemplate(String pattern, Map<String, Object> kv_storage) {
        final Pattern p = Pattern.compile("\\{(\\p{N}*\\p{L}*\\p{N}*)\\}");
        Matcher m = p.matcher(pattern);
        StringBuffer out = new StringBuffer();

        while (m.find())
            if (kv_storage.containsKey(m.group(1))) m.appendReplacement(out, kv_storage.get(m.group(1)).toString());
            else m.appendReplacement(out, m.group(0));

        m.appendTail(out);
        return out.toString();
    }

    /**
     * Генерирует рандомную строку определенной длинны
     *
     * @param count {@link Integer} Длинна строки
     * @return {@link String} Рандомную строку определеной длины
     */
    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        ThreadLocalRandom localRandom = ThreadLocalRandom.current();
        while (count-- != 0) {
            int character = localRandom.nextInt(ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    /**
     * Хеширует введенные парамметры использую перец и соль алгоритмом {@code SHA-384}
     *
     * @param message {@link String} То, что нужно захешировать
     * @param salt    {@link String} Соль, для усложнения расшифровки захешированного сообщения
     * @return {@link String} Захешированная строка
     */
    public static String getSHA384(String message, String salt) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-384");
            byte[] messageDigest = md.digest((PEPPER + message + salt).getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            StringBuilder hashtext = new StringBuilder(no.toString(16));
            while (hashtext.length() < 32) hashtext.insert(0, "0");
            return hashtext.toString();
        } catch (NoSuchAlgorithmException salge) {
            if (Main.isDebug) salge.printStackTrace();
        }
        return "";
    }

    /**
     * Проверяет валидность e-mail почты
     *
     * @param email {@link String}. Почта, которую надо проверить на валидность
     * @return {@link Boolean}. true если валидная, иначе false.
     */
    public static boolean isValidEmail(String email) {
        return Pattern.matches(PATTERN_EMAIL, email);
    }
}
