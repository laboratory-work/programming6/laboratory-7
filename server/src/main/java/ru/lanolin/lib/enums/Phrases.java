package ru.lanolin.lib.enums;

/**
 * Фразы, которые говорят {@link ru.lanolin.lib.human.Human} при общении друг с другом
 */
public enum Phrases {

    FEELGOOD("Как хорошо, что нет дождя"),
    FIND("Как хорошо, что сумка нашлась."),
    OTHER("Как хорошо.");

    private final String phrases;

    Phrases(String phrases) {
        this.phrases = phrases;
    }

    public String getPhrases() {
        return phrases;
    }
}
