package ru.lanolin.lib.human;

import ru.lanolin.lib.enums.Feel;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;

/**
 * Описнаие участника мероприятия
 */
public abstract class Human implements DoHuman, Comparable<Human>, Serializable, Comparator<Human> {

    private static final long serialVersionUID = -2186644049879053315L;

	/**
     * Имя человека
     */
    private String name;

    /**
     * Настроение человека
     */
    private Feel feel;

    /**
     * Качество наряда на человеке
     */
    private int quality;

    /**
     * Дата инициализации коллекции
     */
    private LocalDateTime birthday;

    private transient int creator;

    public Human(String name, Feel feel, int quality, LocalDateTime date, int creator) {
        this(name, feel, creator);
        this.quality = quality;
        this.birthday = date;

    }

    public Human(String name, Feel feel, int creator) {
        this(creator);
        this.feel = feel;
        this.name = name;
    }

    protected Human(int creator) {
        birthday = LocalDateTime.now();
        this.quality = 1;
        this.creator = creator;
    }

    @Override
    public String toString() {
        //("name", creator, feel, quality, birthday)
        return String.format("Name: %s, Fate: %s, Quality: %d, Birthday: %s",
                name, feel.toString(), quality, birthday.toLocalDate().toString());
    }

//    {"name": "KKKK", "fate": "NONE", "quality": 55, "birthday": "2012-01-21"}
    @Override
    public int hashCode() {
        int sum = toString().hashCode() + 23 * creator;
        if (feel == Feel.CONSTRAINT) sum += 5 * 101;
        else if (feel == Feel.NONE) sum += 2 * 101;
        else if (feel == Feel.OK) sum += 3 * 101;

        return sum;
    }

    /**
     * Сравнивает двух {@link Human}. Сначало именя, потом насероение, потом качество и последнее - оценка
     *
     * @param another {@link Human}
     * @return -1 if {@code this} less {@code another} <br>
     * 0 if {@code this} equals {@code another} <br>
     * 1 if {@code this} grate {@code another}
     */
    @Override
    public int compare(Human o1, Human another) {
        int compare = o1.name.compareTo(another.getName());
        if (compare != 0) return compare;

        compare = o1.feel.compareTo(another.getFeel());
        if (compare != 0) return compare;

        compare = o1.getBirthday().compareTo(another.getBirthday());
        if (compare != 0) return compare;

        return Integer.compare(o1.quality, another.getQuality());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || this.getClass() != obj.getClass()) return false;
        Human h = (Human) obj;
        return this.feel == h.getFeel() &&
                this.name.equals(h.getName()) &&
                this.quality == h.getQuality() &&
                this.birthday.equals(h.getBirthday()) &&
                this.creator == h.getCreator();
    }

    /**
     * Сравнивает двух {@link Human} по оценке за наряд
     *
     * @param another {@link Human}
     * @return -1 if {@code this} less {@code another} <br>
     * 0 if {@code this} equals {@code another} <br>
     * 1 if {@code this} grate {@code another}
     */
    @Override
    public int compareTo(Human another) {
        return Integer.compare(this.getQuality(), another.getQuality());
    }

    /**
     * Оценит наряд другого {@link Human}.
     *
     * @param other {@link Human}
     */
    public abstract void rateOutfit(Human other);

    public Feel getFeel() {
        return feel;
    }

    public String getName() {
        return name;
    }

    public int getQuality() {
        return quality;
    }

    public LocalDateTime getBirthday() {
        return birthday;
    }

    public int getCreator() {
        return creator;
    }
}