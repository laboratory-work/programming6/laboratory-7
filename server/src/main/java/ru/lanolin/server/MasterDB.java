package ru.lanolin.server;

import ru.lanolin.additions.Guest;
import ru.lanolin.lib.enums.Feel;
import ru.lanolin.lib.fete.Fete;
import ru.lanolin.lib.human.Human;
import ru.lanolin.util.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

import static ru.lanolin.Main.isDebug;

/**
 * Универсальный класс, для работы с базами данными
 *
 * @version 1.3
 */
public class MasterDB {

    /**
     * SQL запрос для добавления нового пользователя.
     */
    private static final String ADD_NEW_USER = "insert into authentication(email, login, passwd, salt) values (?, ?, ?, ?);";

    /**
     * SQL запрос для добавления нового {@link Human} в БД
     */
    private static final String ADD_NEW_HUMAN = "insert into human(\"name\", creator, feel, quality, birthday)" +
            "values (?, ?, ?, ?, ?) ON CONFLICT (\"name\") " +
            "do update set feel = EXCLUDED.feel, quality = EXCLUDED.quality, birthday = EXCLUDED.birthday;";

    /**
     * SQL запрос для удаления {@link Human} из БД
     */
    private static final String DELETE_HUMAN = "DELETE FROM human WHERE \"name\" = ?";

    /**
     * SQL запрос для получения всех записей для создания {@link Human}
     */
    private static final String GET_ALL_HUMAN = "select \"name\", feel, quality, birthday, creator from human;";

    /**
     * SQL запрос для проврки на ссуществования пользователя в БД при регистарции
     */
    private static final String CHECK_USER = "select count(id) from authentication where email = ? or login = ?;";

    /**
     * SQL запрос для получения записи об пользователе
     */
    private static final String AUTHENTICATION = "select id,passwd,salt from authentication where login = ?;";


    /*
    ALTER SEQUENCE product_id_seq RESTART WITH 1453
    insert into s264472.human(creator, "name", birthday, feel, quality) values (1, 'Sarah Livingston', '2000-12-21T00:00:00', 'NONE', 1),
(1, 'Paola Zhang', '2017-07-13T00:00:00', 'CONSTRAINT', 3),
(1, 'Alisa Galvan', '1990-11-20T00:00:00', 'OK', 13),
(1, 'Maud Lugo', '1969-10-02T00:00:00', 'NONE', 14),
(1, 'Natalya Bernard', '2009-03-10T00:00:00', 'OK', 17),
(1, 'Ally Chung', '1984-09-06T00:00:00', 'NONE', 23),
(1, 'Tanvir Powers', '2001-04-03T00:00:00', 'OK', 43),
(1, 'Enya Conner', '1959-09-28T00:00:00', 'CONSTRAINT', 51),
(1, 'Lincoln Collins', '2007-07-18T00:00:00', 'OK', 55),
(1, 'Jamison Aguilar', '1993-07-01T00:00:00', 'CONSTRAINT', 55),
(1, 'Rayna Hendrix', '1986-05-05T00:00:00', 'CONSTRAINT', 56),
(1, 'Zane Hoffman', '1970-02-19T00:00:00', 'OK', 64),
(1, 'Andreas Camacho', '2014-09-17T00:00:00', 'NONE', 70),
(1, 'Divine Werner', '1956-11-29T00:00:00', 'NONE', 72),
(1, 'Eden Collins', '2017-03-14T00:00:00', 'NONE', 81),
(1, 'Kurtis Dejesus', '1960-06-01T00:00:00', 'OK', 95);
     */

    //SELECT authentication.login , human."name", human.feel, human.quality, human.birthday
    //	FROM s264472.human,s264472.authentication WHERE human.creator = authentication.id;

    /**
     * {@link Connection} для соединения с БД
     */
    private Connection connection;

    /**
     * Существует ли подключение к БД
     */
    private boolean startDB = false;

    /**
     * Настройки приложения.
     */
    private final Properties configuration;

    {
        configuration = new Properties();
        try {
            InputStream propertyFile = ru.lanolin.Main.class.getClassLoader().getResourceAsStream("application.properties");
            assert propertyFile != null;
            configuration.load(propertyFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод, для содания подключения к БД.
     *
     * @return {@link Boolean}. {@code true} если успешно создано подключение
     */
    public boolean connect() {
        if (!startDB) try {
            Class.forName(configuration.getProperty("database.driver", "org.postgresql.Driver"));

            connection = DriverManager.getConnection(configuration.getProperty("database.url", ""), configuration.getProperty("database.user", ""), configuration.getProperty("database.password", ""));

            startDB = true;
        } catch (Exception e) {
            System.err.println("Невозможно подключиться к БД. Указаны неправильные параметры");
            if (isDebug) e.printStackTrace();
            startDB = false;
        }
        return startDB;
    }

    /**
     * Метод, для прекращения работы с БД.
     */
    public void disconnect() {
        if (startDB) try {
            if (!connection.isClosed()) connection.close();
            startDB = false;
        } catch (Exception e) {
            if (isDebug) e.printStackTrace();
        }
    }

    /**
     * Проверка, есть ли подключение к БД.
     * @return {@link Boolean}. {@code true} если успешно создано подключение
     */
    public boolean isConnect() {
        return startDB;
    }

    /**
     * Получает всех восстановленных пользователей из БД
     * @return {@link ArrayList} всех воссозданных {@link Human}
     */
    public ArrayList<Human> getAllHumans() {
        ArrayList<Human> human = new ArrayList<>();

        if (startDB)
            try {
                PreparedStatement PSGetAllHuman = connection.prepareStatement(GET_ALL_HUMAN);
                ResultSet result = PSGetAllHuman.executeQuery();
                while (result.next()) {
                    Guest g = new Guest(result.getString("name"), Feel.valueOf(result.getString("feel")), result.getInt("quality"), result.getString("birthday") + "T00:00:00", result.getInt("creator"));
                    human.add(g);
                }
                result.close();
                PSGetAllHuman.close();
            } catch (Exception e) {
                if (isDebug) e.printStackTrace();
            }

        return human;
    }

    /**
     * Проверяет введенные параметры на совпадение с БД
     * @param login {@link String} логин пользователя консоли
     * @param passwd {@link String} пароль пользователя консои
     * @return {@link Integer} идентификатор пользователя
     */
    public int isAutentification(String login, String passwd) {
        if (startDB) {
            try {
                PreparedStatement PSAuthentication = connection.prepareStatement(AUTHENTICATION);
                PSAuthentication.setString(1, login);
                ResultSet result = PSAuthentication.executeQuery();
                while (result.next()) {
                    int id = result.getInt("id");
                    String passwdS = result.getString("passwd").trim();
                    String saltS = result.getString("salt").trim();
                    String hex = Utils.getSHA384(passwd, saltS).trim();
                    if (hex.equals(passwdS)) {
                        return id;
                    }
                }
                result.close();
                PSAuthentication.close();
            } catch (Exception e) {
                if (isDebug) e.printStackTrace();
            }
        }
        return -1;
    }

    /**
     * Проверяет, существует пользватель с таким email и login в БД
     * @param email {@link String} e-mail пользователя
     * @param login {@link String} login пользователя
     * @return {@link Boolean} true если в бд содержиться записи
     */
    public boolean checkAuterization(String email, String login) {
        if (startDB) {
            try {
                PreparedStatement PSCheckUser = connection.prepareStatement(CHECK_USER);
                PSCheckUser.setString(1, email);
                PSCheckUser.setString(2, login);
                ResultSet result = PSCheckUser.executeQuery();
                result.next();
                int count = result.getInt("count");
                result.close();
                PSCheckUser.close();
                return count != 0;
            } catch (Exception e) {
                if (isDebug) e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * Заносит данные о новом пользователе в БД
     *
     * @param email
     *        {@link String}. e-mail нового пользователя
     * @param login
     *        {@link String}. login нового пользователя
     * @param passwd
     *        {@link String}. pssword нового пользователя
     */
    public void registrationNewAccount(String email, String login, String passwd) {
        if (startDB) {
            try {
                PreparedStatement PSAddNewUser = connection.prepareStatement(ADD_NEW_USER);
                String salt = Utils.randomAlphaNumeric(10);
                PSAddNewUser.setString(1, email);
                PSAddNewUser.setString(2, login);
                PSAddNewUser.setString(3, Utils.getSHA384(passwd, salt));
                PSAddNewUser.setString(4, salt);
                PSAddNewUser.execute();
                PSAddNewUser.clearParameters();
                PSAddNewUser.close();
            } catch (SQLException e) {
                if (isDebug) e.printStackTrace();
            }
        }
    }

    /**
     * Вносит изменения об {@link Human} из класса {@link Fete} в БД
     * @param fete {@link Fete}. Готовое мероприятие
     */
    public void saveHuman(Fete fete) {
        if (startDB && Objects.nonNull(fete)) {
            final List<Human> notModif = getAllHumans();
            final List<Human> modifier = fete.getGuests();

            List<Human> deleted = new ArrayList<>(notModif);
            deleted.removeAll(modifier);

            try {
                PreparedStatement PSDeleteHuman = connection.prepareStatement(DELETE_HUMAN);
                deleted.forEach(human -> {
                    try {
                        PSDeleteHuman.setString(1, human.getName());
                        PSDeleteHuman.addBatch();
                    } catch (SQLException e) {
                        if (isDebug) e.printStackTrace();
                    }
                });
                PSDeleteHuman.executeBatch();
                PSDeleteHuman.close();
            } catch (SQLException e) { if (isDebug) e.printStackTrace(); }

            List<Human> newHuman = new ArrayList<>(modifier);
            newHuman.removeAll(notModif);

            try {
                PreparedStatement PSAddNewHuman = connection.prepareStatement(ADD_NEW_HUMAN);
                newHuman.stream().filter(human -> human.getCreator() == fete.getCreator()).forEach(human -> {
                    try {
                        PSAddNewHuman.setString(1, human.getName());
                        PSAddNewHuman.setInt(2, human.getCreator());
                        PSAddNewHuman.setString(3, human.getFeel().toString());
                        PSAddNewHuman.setInt(4, human.getQuality());
                        PSAddNewHuman.setDate(5, java.sql.Date.valueOf(human.getBirthday().toLocalDate()));
                        PSAddNewHuman.addBatch();
                    } catch (SQLException e) {
                        if (isDebug) e.printStackTrace();
                    }
                });

                PSAddNewHuman.executeBatch();
                PSAddNewHuman.close();
            } catch (SQLException e) {
                if (isDebug) e.printStackTrace();
            }
        }
    }
}
