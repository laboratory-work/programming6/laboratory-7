package ru.lanolin.server;

import ru.lanolin.util.Utils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.Properties;

/**
 * Класс, использутеся для отпраки сообщений на e-mail об успешной регистрации. По умолчанию, используется сервис
 * отправки сообщений <a href="https://mailtrap.io/inboxes">mailtrap.io</a> Чтобы изменинть сервис, нужно внести нужные
 * изменения в {@code application.properties}
 *
 * @version 1.2
 */
public class MailSend {

	/**
	 * Адрес, откуда бы отправено сообщение
	 */
	private static final String EMAIL_FROM;
	/**
	 * Название сообщения
	 */
	private static final String TITLE = "Регистрация в приложении";

	/**
	 * Конфигурации всего приложения
	 */
	private static final Properties configuration;
	/**
	 * Конфигурации отправки сообщения
	 */
	private static final Properties properties;

	/**
	 * Сырой вариант сообщения
	 */
	private static final String MAIL_RAW = "<h1>Регистрация аккаунта</h1><h2>Welcome, %1$s!</h2>" +
			"<p>Благодарим за регистрацию на сервере. Теперь у Вас есть доступ к интерактивной консоли сервера.</p>" +
			"<h3>Информация об аккаунте:</h3><p>Login: %1$s</p><p>Password: %2$s</p>";

	/**
	 * Шаблон e-mail сообщения
	 */
	private static String MAIL_HTML;

	/**
	 * Сессия, для сохранения подключения к smtp серверу
	 */
	private static Session session;

	static {
		configuration = new Properties();
		try {
			InputStream propertyFile = ru.lanolin.Main.class.getClassLoader().getResourceAsStream("application.properties");
			configuration.load(propertyFile);
		} catch (IOException e) {
			System.err.println("Ошибка при чтении конфигурационных файлов");
			e.printStackTrace();
		}
		EMAIL_FROM = configuration.getProperty("mail.smtp.email.from", "");

		try {
			InputStream is = Objects.requireNonNull(ru.lanolin.Main.class.getClassLoader().getResourceAsStream("mail/registr.html"));
			byte[] html = new byte[is.available()];
			is.read(html);
			MAIL_HTML = new String(html, Charset.forName("UTF-8"));
			is.close();
		} catch (IOException e) {
			System.err.println("Ошибка при чтении шаблона e-mail сообщения");
			e.printStackTrace();
		}

		properties = new Properties();
		properties.put("mail.smtp.auth", Boolean.parseBoolean(configuration.getProperty("mail.smtp.auth", "")));
		properties.put("mail.smtp.starttls.enable", configuration.getProperty("mail.smtp.starttls.enable", ""));
		properties.put("mail.smtp.host", configuration.getProperty("mail.smtp.host", ""));
		properties.put("mail.smtp.port", configuration.getProperty("mail.smtp.port", ""));
		properties.put("mail.smtp.ssl.trust", configuration.getProperty("mail.smtp.host", ""));
		properties.put("mail.mime.charset", configuration.getProperty("mail.mime.charset", ""));
		properties.put("mail.smtp.socketFactory.port", configuration.getProperty("mail.smtp.socketFactory.port", ""));
		properties.put("mail.smtp.socketFactory.class", configuration.getProperty("mail.smtp.socketFactory.class", ""));

	}

	/**
	 * Инициализация подкючения
	 */
	public static void init() {
		session = Session.getInstance(properties, new MailAuthenticationPassword());
	}

	/**
	 * Отправка сообщения
	 *
	 * @param email
	 *        {@link String} адрес, куда отправлять
	 * @param login
	 *        {@link String} логин зарегестрированного пользователя
	 * @param passwd
	 *        {@link String} пароль зарегестрированного пользоваателя
	 * @throws Exception
	 * 		Возникает при ошибочных данных или при невозможности отправки сообщения
	 */
	public static void sendMessage(String email, String login, String passwd) throws Exception {
		if (session == null) throw new Exception("Внимание! Не произведена инициализация сессии");

		String msg = Utils.convertTemplate(MAIL_HTML, "login", login, "password", passwd);
		String raw = String.format(MAIL_RAW, login, passwd);

		Message message = new MimeMessage(session);

		message.setFrom(new InternetAddress(EMAIL_FROM));
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
		message.setSubject(TITLE);

		MimeBodyPart text_html = new MimeBodyPart();
		text_html.setContent(msg, "text/html; charset=UTF-8");

		MimeBodyPart text_plain = new MimeBodyPart();
		text_plain.setContent(raw, "text/plain; charset=UTF-8");

		Multipart multipart = new MimeMultipart(text_plain, text_html);

		message.setContent(multipart);
		Transport.send(message);
	}

	/**
	 * Класс, для аутентификации на smtp сервере с помощью пароля
	 */
	private static class MailAuthenticationPassword extends Authenticator {
		@Override
		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(
					configuration.getProperty("mail.smtp.username", ""),
					configuration.getProperty("mail.smtp.password", "")
			);
		}
	}
}
