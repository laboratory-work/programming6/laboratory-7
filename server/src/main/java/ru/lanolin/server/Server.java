package ru.lanolin.server;

import ru.lanolin.server.thread.OneClientAnswThread;

import java.io.IOException;
import java.io.InputStream;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import static ru.lanolin.Main.isDebug;

/**
 * Используется для создание потока, который слушает {@link ServerSocketChannel} на опреленном порту, который получат из
 * файла конфиурации
 *
 * @version 2.1
 */
public class Server extends Thread {

    /**
     * Указывается, какой порт прослушивать
     */
    private final InetSocketAddress socketAddress;
    /**
     * Регистрирует {@link SelectionKey} в определенный доступ
     */
    private Selector selector;
    /**
     * Прослушавает сокет на момент подключения
     */
    private ServerSocketChannel socketChannel;
    /**
     * Проверяет, что подключенный пользователь в режиме {@link SelectionKey#OP_WRITE} При наличии активного
     * подключения, заносится в {@link ArrayList} и создается поток {@link OneClientAnswThread} отправки данных на
     * клиент. <br> При повторной попытке создать поток, он не создатся
     */
    private ArrayList<SocketChannel> available = new ArrayList<>();

    {
        Properties configuration = new Properties();
        try {
            InputStream propertyFile = ru.lanolin.Main.class.getClassLoader().getResourceAsStream("application.properties");
            assert propertyFile != null;
            configuration.load(propertyFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        socketAddress = new InetSocketAddress(Integer.parseInt(
                configuration.getProperty("server.port", "2504")
        ));
    }

    /**
     * Конструктор. Создается, открывается и bind {@link Selector} и {@link ServerSocketChannel}
     */
    public Server() {
        super("[Server]");
        try {
            selector = Selector.open();

            socketChannel = ServerSocketChannel.open();
            socketChannel.bind(socketAddress);
            socketChannel.configureBlocking(false);
            socketChannel.register(selector, socketChannel.validOps(), null);
        } catch (BindException be) {
            System.err.println("Внимание! " + be.getLocalizedMessage());
            try {
                selector.close();
                socketChannel.close();
                System.exit(0);
            } catch (IOException e) {
                if (isDebug) e.printStackTrace();
            }
        } catch (IOException e) {
            if (isDebug) e.printStackTrace();
        }
    }

    /**
     * @return {@link Boolean}. {@code true} если {@link Selector} и {@link ServerSocketChannel} открыты. Иначе {@code
     * false}
     */
    public boolean isConnect() {
        return selector.isOpen() && socketChannel.isOpen();
    }

    /**
     * Метод, ответсвенный за прослушивания {@link ServerSocketChannel} на клиентов.
     * При подключении, создание отока отправки сообщения.
     */
    @Override
    public void run() {
        super.run();
        try {
            System.out.println("Сервер запущен");
            while (selector.isOpen() && socketChannel.isOpen() && !isInterrupted()) {
                selector.select();
                Set<SelectionKey> selectionKeys = selector.selectedKeys();
                Iterator<SelectionKey> selectionKeyIterator = selectionKeys.iterator();

                while (selectionKeyIterator.hasNext() && selector.isOpen() && socketChannel.isOpen()) {
                    SelectionKey myKey = selectionKeyIterator.next();

                    if (myKey.isAcceptable()) {
                        SocketChannel socketClient = socketChannel.accept();
                        socketClient.configureBlocking(false);
                        socketClient.register(selector, SelectionKey.OP_WRITE);
                        System.out.println("Connection Accepted: " + socketClient.getRemoteAddress());
                    } else if (myKey.isWritable()) {
                        SocketChannel sc = (SocketChannel) myKey.channel();
                        if (!available.contains(sc)) {
                            new OneClientAnswThread(selector, sc).start();
                            available.add(sc);
                        }
                    } else if (myKey.isConnectable()) {
                        SocketChannel sc = (SocketChannel) myKey.channel();
                        available.remove(sc);
                    }
                    selectionKeyIterator.remove();
                }
            }
        } catch (Exception e) {
            if (isDebug) e.printStackTrace();
        }
    }

    /**
     * Метод, закрывающий все соединения отсанавливает поток.
     */
    @Override
    public void interrupt() {
        try {
            if (socketChannel.isOpen()) socketChannel.close();
            if (selector.isOpen()) selector.close();
        } catch (IOException e) {
            if (isDebug) e.printStackTrace();
        }
        super.interrupt();
    }
}
