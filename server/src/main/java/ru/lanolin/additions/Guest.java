package ru.lanolin.additions;

import ru.lanolin.lib.enums.Feel;
import ru.lanolin.lib.human.Human;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Guest extends Human {

    private static final long serialVersionUID = 4673673961563758015L;

    public Guest(String name, Feel feel, int quality, String date, int creator) {
        super(name, feel, quality, LocalDateTime.parse(date), creator);
    }

    public Guest(String name, Feel feel, int creator) {
        super(name, feel, creator);
    }

    public Guest(int creator) {
        super(creator);
    }

    @Override
    public void rateOutfit(Human other) {
        System.out.println(">" + this.getName() + " оцениват наряд Гостя " + other.getName() + " на " + other.getQuality());
    }

    @Override
    public void great(Human other) {
        System.out.println(">" + this.getName() + " приветвует " + other.getName());
    }

    @Override
    public void bowOut(Human other) {
        System.out.println(">" + this.getName() + " поклонился " + other.getName());
    }
}