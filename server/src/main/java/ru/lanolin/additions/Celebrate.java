package ru.lanolin.additions;

import ru.lanolin.lib.exception.AddHumanException;
import ru.lanolin.lib.exception.LeaveHumanException;
import ru.lanolin.lib.fete.Fete;
import ru.lanolin.lib.human.Human;

import java.util.List;

public class Celebrate extends Fete {

	public Celebrate(List<Human> databases) {
		super(databases);
    }

    @Override
    public void addParticipant(Human h) {
        if (this.guests.contains(h)) throw new AddHumanException();
        this.guests.add(h);
    }

    @Override
    public void leaveParticipant(Human h) {
        if (!this.guests.contains(h)) throw new LeaveHumanException();
        this.guests.remove(h);
    }
}
