# Лабораторная работа №7 по программированию

## Задача
Доработать лабораторную работу №6:
1. В класс, объекты которого хранятся в коллекции, добавить поле типа java.time.LocalDateTime, в котором должны храниться дата и время создания объекта.
2. Обеспечить возможность регистрации и авторизации пользователей.
3. При регистрации генерировать случайный пароль и отправлять его на почту, указанную при регистрации.
4. Для отправки почтовых уведомлений использовать JavaMail API.
5. Пароли при хранении хэшировать алгоритмом SHA-384.
6. Реализовать ассоциацию между объектом из коллекции и пользователем, его создавшим. Пользователи могут просматривать объекты, созданные всеми пользователями, а модифицировать - только свои.
7. Для идентификации пользователя отправлять логин и пароль с каждым запросом.
8. Обеспечить хранение всех данных (объектов коллекции, пользователей и ассоциаций) в реляционной базе данных

Порядок выполнения работы:
- В качестве базы данных использовать PostgreSQL.
- Для подключения к БД на кафедральном сервере использовать хост pg, имя базы данных - studs, имя пользователя/пароль совпадают с таковыми для подключения к серверу.
- Данные для подключения к почтовому серверу уточняются.

## Общий список заданий:
- Реализовать тесты

#### БД
1. Таблица **authentication**:
    + *ID* -> integer, primary key
    + *login* -> varchar(64), unique
    + *e-mail* -> varchar(64), uniquq
    + *passwd* -> varchar(256)
    + *salt* -> varchar(5)
2. Таблица **human**:
    + *ID* -> integer, primary key
    + *name* -> text, unique
    + *creator* -> varchar(64), extend key
    + *Feel* -> varchar(15) 
    + *quality* -> integer
    + *birthday* -> date
3. Таблица **joke**:
    + *id* -> integer, primary key
    + *joke* -> text
    + *author* -> text